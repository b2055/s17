console.log("Array Manipulation");

// Basic Array Structure
// Access Elements in an Array - through index

// two ways to initialize an Array

let array = [1, 2, 3];
console.log(array)

let arr = new Array(1, 2, 3);


// Array Manipulation

let count = ["one", "two", "three", "four"]

// Q: How are we going to add a new element at the end of an array

console.log(count.length)
console.log(count[4])

	// using assignment operator (=)
	count[4] = "five";
	console.log(count);

	// push method array.push()
		//add element at the end of an array
	count.push("element");
	console.log(count);
	
	function pushMethod(element) {
		return count.push(element)
	}

	pushMethod("six")
	pushMethod("seven")
	pushMethod("eight")

	console.log(count)

	// pop method array.pop()
	// removes the last element of an array
	count.pop();

	console.log(count)

	// Q: Can we use pop method to remove "four" element?
 	// - No, it will still remove the last element no matter what
	count.pop("four");
	console.log(count)

	function popMethod() {
		count.pop();
	}
	popMethod();

	console.log(count)


	// Q: how do we add element at the beginning of an array

		// Unshift Method array.unshift()
			//add element at the beginning of an array
		count.unshift("hugot");
		console.log(count)

		function unshiftMethod(name) {
			return count.unshift(name);
		}

		unshiftMethod("zero");

		console.log(count)

		// Can we also remove element at the beginning of an array? 
		// -
			// shift method array.shift();
				//removes an element at the beginning of an array
			count.shift();
			console.log(count);

			function shiftMethod() {
				count.shift();
			}

			shiftMethod();

			console.log(count)

		// sort method array.sort();
		let nums = [15, 232, 34, 4222, 224, 67];

		nums.sort();
		console.log(nums)


		// sort nums in ascending order

		nums.sort(
			function(a, b) {
				return a - b
			}
		)

		console.log(nums)

		// sort nums in descending order

		nums.sort(
			function(a, b) {
				return b - a
			}
		)

		console.log(nums)

		// reverse method array.reverse()
		nums.reverse()

		console.log(nums);


		/*Splice and slice*/


		// Splice method array.splice()
			//returns an array of omitted elements
			// it directly manipulates the original array 

			// first parameter - index where to start omitting element
			// second parameter - # of elements to be omitted starting from first parameter
			// third parameter - elements to be added in place of the ommited elements

		// let newSplice = count.splice(1);
		// console.log(newSplice)

		// let newSplice = count.splice(1, 2);
		// console.log(newSplice);

		// let newSplice = count.splice(1, 2, "a1", "b2")
		// console.log(newSplice)
		// console.log(count)


		// Slice method array.slice()

			// first parameter - index where to begin omitting elements
			// second parameter - # of elements to be omitted (index - array.lenght)

		// console.log(count)
		// let newSlice = count.slice(1);
		// console.log(newSlice)

		let newSlice = count.slice(1, 5)
		console.log(newSlice)
		console.log(count)

		// Concat method array.concat()
			// used to merge two or more arrays
		console.log(count)
		console.log(nums)

		let animals = ["bird", "cat", "dog", "fish"];

		let newConcat = count.concat(nums, animals)

		console.log(newConcat);

		// Join method array.join()
		// "", "-", " "
		let meal = ["rice", "steak", "juice"];

		let newJoin = meal.join()
		console.log(newJoin)

		newJoin = meal.join("")
		console.log(newJoin)

		newJoin = meal.join(" ")
		console.log(newJoin)

		newJoin = meal.join("-")
		console.log(newJoin)


		// toString method array.toString()

		console.log(nums)
		console.log(typeof nums[3])

		let newString = nums.toString()

		console.log(typeof newString)


/*Accessors*/

let countries = ["US", "PH", "CAN", "SG", "HK", "PH", "NZ"];

	//indexOf() array.indexOf()

		//finds the index of a given element where it is "first" found
	let index = countries.indexOf("PH")
	console.log(index)

		//if element is non existing, return is -1
	index = countries.indexOf("AU")
	console.log(index)


	// lastIndexOf()
	let lastIndex = countries.lastIndexOf("PH")
	console.log(lastIndex)

/*	if (countries.indexOf("AU") == -1) {
		console.log("Element not existing")
	}
	else {
		console.log("Element exists in the array")
	}*/


/*Iterators*/
	// forEach(callback()) array.forEach()
	// map() array.map()

let days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];

	let daysOftheweek = days.forEach(
		function(element){
			console.log(element)
		}
	)


	// map
	let mapDays = days.map(function(day){
		return `${day} is the day of the week`
	})

	console.log(mapDays)
	console.log(days)


	// mini activity

	let days2 = new Array();

	days.forEach(function(days){
		days2.push(days);
	})

	console.log(days2)


	// filter method array.filter(cb())

	console.log(nums)

	let newFilter = nums.filter(function(num) {
		return (num < 50)
	})

	console.log(newFilter)

	// includes array.includes()
		//returns boolean if val is found

		console.log(animals)

		let newIncludes = animals.includes("dog")

		console.log(newIncludes)

		// mini activity

		let checker = "";

		function isFound(val) {
			checker = animals.includes(val)

			if (checker === true) {
				console.log(val + " is found")
			}
			else if (checker === false) {
				console.log(val + " is not found")
			}
		}

		isFound("dog");
		isFound("cat");
		isFound("lion");



		// every(cb())

			console.log(nums)
			// boolean
			// returns true only if "all elements" passed the given condition

			let newEvery = nums.every(function(num){
				return (num > 10)
			})

			console.log(newEvery);


		// some(cb())
			// boolean

			let newSome = nums.every(function(num){
				return (num > 50)
			})
			console.log(newSome);

			newSome2 = nums.some(num => num > 50)
			console.log(newSome)


		// reduce(cb(<previous>, <current>))
		
		let newReduce = nums.reduce(function(a, b){
			return a + b
			// return b - a
		})

		console.log(newReduce)

		// to get the average of nums array
			// total all the elements
			// divide it by total number of elements

		let average = newReduce/ nums.length;

		//
			

		// toFixed(# of decimals) - returns string

		console.log(average.toFixed(2))

		// parseInt() or parseFloat()

		console.log(parseInt(average.toFixed(2)))
		console.log(parseFloat(average.toFixed(2)))