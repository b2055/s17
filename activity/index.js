
// Activity Array Manipulation

let student = new Array();

function addStudent(name) {
	student.push(name);
}

	addStudent("Elesis")
	addStudent("Lire")
	addStudent("A-two")
	addStudent("Arme")
	addStudent("Mashiro")
	addStudent("Mashiro")


function countStudent() {
	return student.length;
}

	console.log("Student Count: " + countStudent())


let sortedStudent = new Array()

function printStudents() {

	sortedStudent = student.sort((a, b) => a.localeCompare(b))


	console.log("Sorted Students:")
	sortedStudent.forEach((student) => console.log(student))
}


printStudents();


// new studentFilter Array
let studentFilter = new Array();


// findStudent Function
function findStudent(search) {

	// search value to lower case 
	search = search.toLowerCase();

	// studentFilter array = student array - na nakafilter
	studentFilter = student.filter(function(keyWordFilter) {

		// titignan ang value ni "search" kung meron ba nito sa student array
		// lower case din ang keyWordFilter
		return search === keyWordFilter.toLowerCase();
	})

	// kung walang nakitang "search" value sa student array
	if(studentFilter == null || studentFilter == 0) {
		console.log(search + " is not an enrollee")
	} 
	else {

		// kung may nakitang isa "search" value sa student array
		if (studentFilter.length === 1) {
			console.log(search + " is an enrollee")
		} else {
		// kung may nakitang marami "search" value sa student array
			console.log(search + " are enrollees")
		}

	}

}

// tawagin mo findStudent(search)
findStudent("mashiro");


let sectionStudents = new Array();

function addSection(section) {
	sectionStudents = student.map((studentMap) => `${studentMap} - ${section}`)

	console.log(sectionStudents)
}

addSection("Section A");


function removeStudent(studentValue) {

	studentValue = studentValue[0].toUpperCase() + studentValue.slice(1);

	let studentIndex = student.indexOf(studentValue)

	let studentSplice = student.splice(studentIndex, 1);

}

removeStudent("a-two");
console.log(student)